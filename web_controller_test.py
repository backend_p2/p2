import requests
import hashlib
import random
import os
import json


BASE_URL = 'http://127.0.0.1:8000'
STATUS_OK = requests.codes['ok']
STATUS_BAD_REQUEST = requests.codes['bad_request']
STATUS_NOT_FOUND = requests.codes['not_found']


def test_upload():
    data = open("test.tar.gz", 'rb').read()
    MD5 = hashlib.md5(data).hexdigest()
    name = "pack-" + MD5 + "-" + "test.tar.gz"
    resp = requests.post(BASE_URL + "/buckettest/test.tar.gz", data=data)
    assert resp.status_code == STATUS_OK
    assert json.loads(resp.content)["output_name"] == name
    resp = requests.get(BASE_URL + "/bud/test.tar.gz")
    assert resp.status_code == STATUS_BAD_REQUEST
    resp = requests.post(BASE_URL + "/buckettest/test.tar.gz")
    assert resp.status_code == STATUS_BAD_REQUEST
    assert json.loads(resp.content)["output_name"] == name


def test_download():
    return
