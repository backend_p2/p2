import requests
import hashlib
import random
import os
import json
import time



# http://192.168.99.100:30008/test/tttt.tar.gz
BASE_URL = 'http://192.168.99.100:30008'
STATUS_OK = requests.codes['ok']
STATUS_BAD_REQUEST = requests.codes['bad_request']
STATUS_NOT_FOUND = requests.codes['not_found']

# def test_get_status():
#     """GET /status should have status_code 200"""
#     resp = requests.get(BASE_URL + '/all')
#     assert resp.status_code == STATUS_OK

# def test_create_delete_bucket():

# 	resp = requests.post(BASE_URL + '/buckettest?create')
# 	print (resp.content)
# 	assert resp.status_code == STATUS_OK

# 	resp_delete = requests.delete(BASE_URL + '/buckettest?delete')
# 	print (resp_delete.content)
# 	assert resp_delete.status_code == STATUS_OK

# def test_get_status():
#     """GET /status should have status_code 200"""
#     resp = requests.delete(BASE_URL + '/deleteall')
#     assert resp.status_code == STATUS_OK


def test_uploading():
	objectName = "tt.tar.gz"
	
	for i in range(30):
		D = open('./test.tar.gz', 'rb')
		headers = {'Content-Type' : 'application/x-tar+gzip'}
		resp = requests.post(url=BASE_URL+'/example00'+str(i)+'/'+objectName , 
			data=D,
			headers=headers)
		output =  json.loads(resp.text)["output_name"]

		assert resp.status_code == STATUS_OK

		get_res = requests.get(url=BASE_URL+'/example00'+str(i)+'/'+output)

		while requests.get(url=BASE_URL+'/example00'+str(i)+'/'+output).status_code != STATUS_OK:
			time.sleep(0.100)
