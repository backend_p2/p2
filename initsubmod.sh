#!/bin/sh
cd converter
git pull
git submodule update --remote --recursive
git add . 
git commit -m $1
git push
cd ..
cd job_dispatcher 
git pull
git submodule update --remote --recursive
git add . 
git commit -m $1
git push
cd ..
cd status 
git pull
git submodule update --remote --recursive
git add . 
git commit -m $1
git push
cd ..
cd packer 
git pull
git submodule update --remote --recursive
git add . 
git commit -m $1
git push
cd ..
cd web_controller 
git pull
git submodule update --remote --recursive
git add . 
git commit -m $1
git push
cd ..