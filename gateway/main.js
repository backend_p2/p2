const express = require('express')
const httpProxy = require('express-http-proxy')
const app = express()


app.use('/web-controller', httpProxy('web-controller:8000'));
app.use('/', httpProxy('p2-frontend:5000'));


const port = 80
app.listen(port, () => console.log(`Example app listening on port ${port}!`))


