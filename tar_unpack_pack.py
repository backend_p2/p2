import tarfile
import os

def pdftotext(filename, path):
    pdf = path+"/"+filename+".pdf"
    txt = path+"/"+filename+".txt"
    os.system("pdftotext '%s' '%s'" % (pdf, txt))
    
def pack(path, tar_name):
    with tarfile.open(tar_name, "w:gz") as tar_handle:
        for root, dirs, files in os.walk(path):
            for file in files:
                tar_handle.add(os.path.join(root, file))
            
            
def unpack(tarfilename):
    dir_name = tarfilename.split('.')[0]
    os.mkdir(dir_name)
    path = "./"+dir_name
    with tarfile.open(tarfilename, "r:gz") as tar:
        for member in tar.getmembers():

            if member.isfile():
                name = member.name.split("/")[1]
                filename, extension = os.path.splitext(name)
                if not filename.startswith("._") and extension.lower() == '.pdf':
                    print("found pdf: ", name)
                    tar.extract(member, path)
                    pdftotext(filename, path)
                    os.remove(path+"/"+name)
    pack(path, "txt_"+tarfilename)
    
                
                
tarName = "test.tar.gz"
(unpack(tarName))